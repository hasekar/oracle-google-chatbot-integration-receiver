package com.oraclechatbot.googleassistance.ciberuniversity.entity;

import lombok.Data;

import java.util.Arrays;

@Data
public class OracleOutputRequest {
    private String text;
    private String userId;
    private String choices[];

    public OracleOutputRequest() {
    }

    @Override
    public String toString() {
        return "OracleOutputRequest{" +
                "text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                ", choices=" + Arrays.toString(choices) +
                '}';
    }
}
