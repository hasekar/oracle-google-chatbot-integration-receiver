package com.oraclechatbot.googleassistance.ciberuniversity.entity;

import lombok.Data;

import java.util.Arrays;

@Data
public class DialogFlowResponse {
    private String fulfillmentText;
    private String fulfillmentMessages[];
    private String source;

    public DialogFlowResponse() {
    }

    @Override
    public String toString() {
        return "DialogFlowResponse{" +
                "fulfillmentText='" + fulfillmentText + '\'' +
                ", fulfillmentMessages=" + Arrays.toString(fulfillmentMessages) +
                ", source='" + source + '\'' +
                '}';
    }
}
