package com.oraclechatbot.googleassistance.ciberuniversity.entity;

import lombok.Data;

@Data
public class OracleResponse {
    private String text;
    private String userId;

    public OracleResponse() {
    }

    @Override
    public String toString() {
        return "OracleResponse{" +
                "text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
