package com.oraclechatbot.googleassistance.ciberuniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiberuniversityApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiberuniversityApplication.class, args);
    }
}
