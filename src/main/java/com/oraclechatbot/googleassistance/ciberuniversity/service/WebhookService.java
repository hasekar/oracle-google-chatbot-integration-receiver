package com.oraclechatbot.googleassistance.ciberuniversity.service;

import com.oraclechatbot.googleassistance.ciberuniversity.entity.OracleOutputRequest;
import org.springframework.stereotype.Service;

@Service
public class WebhookService {

    public volatile String responseText = "";

    public void getAndModifyString(OracleOutputRequest oracleOutputRequest) {

        synchronized (this) {
            responseText = "";
            modifyResponse(oracleOutputRequest);
            notify();
        }

        System.out.println("Done string modification "+responseText);

    }

    private void modifyResponse(OracleOutputRequest oracleOutputRequest) {
        if (oracleOutputRequest.getText() != null || !oracleOutputRequest.getText().isEmpty()) {
            responseText += oracleOutputRequest.getText();
        }
        if (oracleOutputRequest.getChoices() != null) {
            if (oracleOutputRequest.getChoices().length > 0) {
                responseText += "\n\nThe following are your choices: \n\n";
                for (String choice : oracleOutputRequest.getChoices()) {
                    responseText += choice + "\n";
                }
                responseText += "\n\nplease type the choices to select it.";
            }
        }
    }


    public void ProcessLoop() throws InterruptedException {


        synchronized(this)
        {
            System.out.println("producer thread running");

            // releases the lock on shared resource
            wait();

            // and waits till some other method invokes notify().
            System.out.println("Resumed");
        }

        System.out.println("to Dialogflow : " + responseText);
    }



}
