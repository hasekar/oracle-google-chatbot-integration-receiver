package com.oraclechatbot.googleassistance.ciberuniversity.controller;


import com.oraclechatbot.googleassistance.ciberuniversity.entity.DialogFlowResponse;
import com.oraclechatbot.googleassistance.ciberuniversity.entity.OracleOutputRequest;
import com.oraclechatbot.googleassistance.ciberuniversity.entity.OracleResponse;
import com.oraclechatbot.googleassistance.ciberuniversity.service.WebhookService;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@CrossOrigin(origins = {"*"},  methods= {RequestMethod.GET, RequestMethod.POST},maxAge = 3600)
@RestController
@RequestMapping("/")
public class webhook {

    private String userId = "Oracle Chatbot - GoogleAssistance Integration";

    private final String URL = "https://oracle-chatbot-google-server.herokuapp.com/message";

    @Autowired
    WebhookService webhookService;

    Thread producer, consumer;


    @GetMapping("/home")
    String testController() {
        return "Testing Application";
    }

    @PostMapping("/webhook")
    String Oracle(@RequestBody OracleOutputRequest request) throws InterruptedException {

         consumer = new Thread(new Runnable() {
            @Override
            public void run() {
                webhookService.getAndModifyString(request);
            }
        });

        consumer.start();

        webhookService.getAndModifyString(request);

        return null;
    }

    @PostMapping("/message")
    DialogFlowResponse dialogFlow(@RequestBody String request) throws JSONException, InterruptedException {

        webhookService.responseText = "";

        JSONObject dialogFlowRequestJsonObject = new JSONObject(request);

        String dialogFlowRequestString =  dialogFlowRequestJsonObject
                .getJSONObject("queryResult").getString("queryText");

        OracleResponse oracleResponse = new OracleResponse();
        oracleResponse.setUserId(userId);
        oracleResponse.setText(dialogFlowRequestString);

        sendToORacleChatBot(oracleResponse);

        producer = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    webhookService.ProcessLoop();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.start();

        producer.join();
        consumer.join();

        DialogFlowResponse dialogFlowResponse = new DialogFlowResponse();
        dialogFlowResponse.setFulfillmentText(webhookService.responseText);
        dialogFlowResponse.setFulfillmentMessages(new String[0]);
        dialogFlowResponse.setSource("www.ciberUniversity.com");

        return dialogFlowResponse;

    }

    String sendToORacleChatBot(OracleResponse requestStringToOracle) {

        String query_url = URL;

        String json = "{ \"text\" : \""+ requestStringToOracle+"\"}";

        try {

            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            System.out.println(result);
            JSONObject myResponse = new JSONObject(result);

            in.close();
            conn.disconnect();

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
}
