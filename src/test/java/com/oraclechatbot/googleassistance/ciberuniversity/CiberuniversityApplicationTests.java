package com.oraclechatbot.googleassistance.ciberuniversity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CiberuniversityApplicationTests {

    @Test
    public void contextLoads() {
    }

}
